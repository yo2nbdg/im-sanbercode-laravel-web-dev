<?php
    require('animal.php');

    $sheep = new Animal("shaun");
    
    echo "Name :  $sheep->name <br>"; // "shaun"
    echo "Legs : $sheep->legs <br>"; // 4
    echo "Cold Blooded : $sheep->cold_blooded <br>"; // "no"

?>